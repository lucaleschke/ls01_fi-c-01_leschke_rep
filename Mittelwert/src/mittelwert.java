import java.util.Scanner; //Schritt 1 = Scanner importieren

public class mittelwert {
	
	public static double verarbeitung(double pZahl1, double pZahl2) {
		
		double ergebnis;
		ergebnis = (pZahl1 + pZahl2) / 2;
		return ergebnis;
	}
	
	public static void main(String[] args) {

		Scanner meinScanner = new Scanner(System.in); //Schritt 2 = Eingabeger�t definiert
		
	      //Deklaration der Variablen
	      double zahl_1;
	      double zahl_2;
	      double mittelwert;
	      
	      // (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      
	      System.out.println("Bitte geben Sie den ersten Wert ein!");
	      zahl_1 = meinScanner.nextDouble();
	      
	      System.out.println("Bitte geben Sie den zweiten Wert ein!");
	      zahl_2 = meinScanner.nextDouble();
	           	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      
	      //mittelwert = (zahl_1 + zahl_2)/2;
	      
	      mittelwert = verarbeitung(zahl_1, zahl_2);
	          
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl_1, zahl_2, mittelwert);
	   }
}
