import java.util.Scanner;

public class AB_1_1 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein!");
		int n = tastatur.nextInt();
		
		for (int i = 1; i < n; i++) {
			System.out.println(i);
		}
		
		for (int i = n; i > 0; i--) {
			System.out.println(i);
		}

	}

}
