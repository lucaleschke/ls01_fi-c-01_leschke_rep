
public class aufgabe4 {

	public static void main(String[] args) {
		
		//aufgabe a
		
		System.out.println("Aufgabe A");
		
		int zahl_a = 99;
		
		while(zahl_a >= 9) {
			zahl_a = zahl_a - 3;
			System.out.println(zahl_a);
		}
		
		//aufgabe b
		
		System.out.println("Aufgabe B");
		
		int zahl_b = 0;
		
		for(int i = 0; zahl_b < 400; i++) {
			
			zahl_b = zahl_b + 2*i + 1;
			
			System.out.println(zahl_b);
		}
		
		//aufgabe c
		
		System.out.println("Aufgabe C");
		
		int zahl_c = -2;
		
		while(zahl_c < 102) {
			zahl_c = zahl_c + 4;
			System.out.println(zahl_c);
		}
		
		//aufgabe d
		
		System.out.println("Aufgabe D");
		
		int zahl_d = 4;
		int addition = 4;
		
		System.out.println(zahl_d);
		
		for(int i = 0; zahl_d < 1024; i++) {
			
			addition = addition + 8;
			zahl_d = zahl_d + addition;
			
			System.out.println(zahl_d);
		}
		
		//aufgabe e
		
		System.out.println("Aufgabe E");
		
		int zahl_e = 1;
		
		while(zahl_e < 32768) {
			zahl_e = zahl_e * 2;
			System.out.println(zahl_e);
		}

	}

}
