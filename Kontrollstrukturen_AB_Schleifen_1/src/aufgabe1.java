import java.util.Scanner;

public class aufgabe1 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Zahl ein!");
		byte n = tastatur.nextByte();
		
		//aufgabe a
		
		for(int i = 1; i <= n; i++) {
			System.out.println(i);
		}
		
		//aufgabe b
		
		for(int i = n; i > 0; i--) {
			System.out.println(i);			
		}

	}

}
