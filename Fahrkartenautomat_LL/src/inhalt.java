import java.util.Scanner;

class inhalt
{	
	public static double fahrkartenbestellungErfassen() {
		double gesamtbetrag = 0;
		double preis = 0;
		double gesamtpreis_1 = 0;
		double gesamtpreis_2 = 0;
		double gesamtpreis_3 = 0;
		int anzahl;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Fahrkartenbestellvorgang:\n" + "===========================\n" + "\n");
	    
	    System.out.println("W�hlen Sie aus:\r\n"
	    		+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
	    		+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
	    		+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
	    		+ "  Bezahlen (9)");
	    
	    System.out.print("Ihre Wahl:");
	    byte ticketAuswahl = tastatur.nextByte();
	    
	    while(ticketAuswahl != 1 && ticketAuswahl != 2 && ticketAuswahl != 3 && ticketAuswahl != 9) {
	    	System.out.println("Falsche Eingabe - bitte w�hlen Sie erneut aus!");
	    	ticketAuswahl = tastatur.nextByte();
	    }
	    
	    while(ticketAuswahl != 9)
	    {
	    
	    switch (ticketAuswahl) {
	    case 1:
	    	preis = 2.90;
	    	
	    	System.out.print("Anzahl der Tickets: ");
		    int anzahl_vor_Check = tastatur.nextInt();
		    //Abfrage ung�ltige Anzahl an Tickets
		    if (anzahl_vor_Check > 0 && anzahl_vor_Check < 11) {
		    	anzahl = anzahl_vor_Check;
		    }
		    else {
		    	int anzahl_nach_Check = 0;
		    	while (anzahl_nach_Check < 1 || anzahl_nach_Check > 10) {
		    		System.out.println("Bitte geben Sie erneut eine Anzahl an Tickets an!");
		    		anzahl_nach_Check = tastatur.nextInt();
		    	}
		    	anzahl = anzahl_nach_Check;
		    }
		    gesamtpreis_1 = preis * anzahl;
		    System.out.println("W�hlen Sie aus:\r\n"
		    		+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
		    		+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
		    		+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
		    		+ "  Bezahlen (9)");
		    System.out.print("Ihre Wahl:");
		    ticketAuswahl = tastatur.nextByte();
	       	break;
	    case 2:
	    	preis = 8.60;
	    	
	    	System.out.print("Anzahl der Tickets: ");
		    anzahl_vor_Check = tastatur.nextInt();
		    //Abfrage ung�ltige Anzahl an Tickets
		    if (anzahl_vor_Check > 0 && anzahl_vor_Check < 11) {
		    	anzahl = anzahl_vor_Check;
		    }
		    else {
		    	int anzahl_nach_Check = 0;
		    	while (anzahl_nach_Check < 1 || anzahl_nach_Check > 10) {
		    		System.out.println("Bitte geben Sie erneut eine Anzahl an Tickets an!");
		    		anzahl_nach_Check = tastatur.nextInt();
		    	}
		    	anzahl = anzahl_nach_Check;
		    }
		    gesamtpreis_2 = preis * anzahl;
		    System.out.println("W�hlen Sie aus:\r\n"
		    		+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
		    		+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
		    		+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
		    		+ "  Bezahlen (9)");
		    System.out.print("Ihre Wahl:");
		    ticketAuswahl = tastatur.nextByte();
	    	break;
	    case 3:
	    	preis = 23.50;
	    	
	    	System.out.print("Anzahl der Tickets: ");
		    anzahl_vor_Check = tastatur.nextInt();
		    //Abfrage ung�ltige Anzahl an Tickets
		    if (anzahl_vor_Check > 0 && anzahl_vor_Check < 11) {
		    	anzahl = anzahl_vor_Check;
		    }
		    else {
		    	int anzahl_nach_Check = 0;
		    	while (anzahl_nach_Check < 1 || anzahl_nach_Check > 10) {
		    		System.out.println("Bitte geben Sie erneut eine Anzahl an Tickets an!");
		    		anzahl_nach_Check = tastatur.nextInt();
		    	}
		    	anzahl = anzahl_nach_Check;
		    }
		    gesamtpreis_3 = preis * anzahl;
		    System.out.println("W�hlen Sie aus:\r\n"
		    		+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
		    		+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
		    		+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)"
		    		+ "  Bezahlen (9)");
		    System.out.print("Ihre Wahl:");
		    ticketAuswahl = tastatur.nextByte();
	    	break;
	    	
	    default:
	    	System.out.println("Fehler!");
	    	break;
	    }	    
	    }
	    gesamtbetrag = gesamtpreis_1 + gesamtpreis_2 + gesamtpreis_3;
	    return gesamtbetrag;    
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   double restwert =  zuZahlenderBetrag - eingezahlterGesamtbetrag;
	    	   
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n", restwert);
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   double eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein/e wird/werden ausgegeben");
		for (int i = 0; i < 15; i++)
	       {
	          System.out.print("=");
	          warte(50);
	       }
	       System.out.println("\n\n");
	}
	
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
	
	public static void rueckgeldAusgeben(double eingezahlterBetrag, double gesamtpreis) {
		double rueckgeld = eingezahlterBetrag - gesamtpreis;
	       if(rueckgeld > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f%s%n " ,  rueckgeld , " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt: ");
	    	   
	    	   // 2 EURO-M�nzen
	    	   if (rueckgeld > 1.99)
	    	   {
		           while(rueckgeld > 1.99) 
		           {
		        	  muenzeAusgeben(2, "Euro");
		        	  //System.out.println("2 EURO");
		        	  rueckgeld -= 2.0;
		           }
	    	   }
		    	   
		       // 1 EURO-M�nzen
	    	   if (rueckgeld > 0.99)
	    	   {
		           while(rueckgeld > 0.99) 
		           {
		        	  muenzeAusgeben(1, "Euro");
		        	  //System.out.println("1 EURO");
		        	  rueckgeld -= 1.0;
		           }
	    	   }
		       
	    	   // 50 Cent-M�nzen
	    	   if (rueckgeld > 0.49)
	    	   {
		           while(rueckgeld > 0.49) 
		           {
		        	  muenzeAusgeben(50, "Cent");
		        	  //System.out.println("50 Cent");
		        	  rueckgeld -= 0.5;
		           }
	    	   }
		       
	    	   // 20 Cent-M�nzen
	    	   if (rueckgeld > 0.19)
	    	   {
		           while(rueckgeld > 0.19) 
		           {
		        	  muenzeAusgeben(20, "Cent");
		        	  //System.out.println("20 Cent");
		        	  rueckgeld -= 0.2;
			          
		           }
	    	   }
	    	   // 10 Cent-M�nzen
	    	   if (rueckgeld > 0.09)
	    	   {
		           while(rueckgeld > 0.09) 
		           {
		        	  muenzeAusgeben(10, "Cent");
		        	  //System.out.println("10 Cent");
		        	  rueckgeld -= 0.1;
		           }
	    	   }
	    	   // 5 Cent-M�nzen
	    	   if (rueckgeld > 0.04)
	    	   {
		           while(rueckgeld > 0.04) 
		           {
		        	  muenzeAusgeben(5, "Cent");
		        	  //System.out.println("5 Cent");
		        	  rueckgeld -= 0.05;
		           }
	    	   }
	    	       	          
	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
           "vor Fahrtantritt entwerten zu lassen!\n"+
           "Wir w�nschen Ihnen eine gute Fahrt." + 
           "\n");
	       
	       System.out.println("======================================" + "\n");
	       System.out.println("Es k�nnen nun weitere Fahrkarten erworben werden." + "\n" + "\n");
	}
	
	
    public static void main(String[] args)
    {
    	boolean fahrkartenkaufm�glich = true;
    	
    	while(fahrkartenkaufm�glich == true)
    	{
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    	}    	    	
    }
}