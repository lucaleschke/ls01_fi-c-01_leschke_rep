package Raumschiff;

public class ladung {

	// Attribute
	private String bezeichnung;
	private int menge;

	/**
	 * Vollparametrisierter Konstruktor der Klasse Ladung
	 * 
	 * @param bezeichnung ist der Name der Ladung
	 * @param menge ist die Anzahl der benannten Ladung
	 */

	public ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * parameterloser Konstruktor f�r die Klasse Ladung
	 */

	public ladung() {

	}

	// Getter und Setter
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	@Override
	public String toString() {
		return "Bezeichnung: " + getBezeichnung() + ", Menge: " + getMenge();
	}
}
