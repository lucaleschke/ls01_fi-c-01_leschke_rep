package Raumschiff;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author Luca Leschke
 * @version 1 vom 25.04.2021
 * 
 */

public class raumschiff {

	// Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<ladung> ladungsverzeichnis = new ArrayList<ladung>();

	/**
	 * Vollparametrisierter Konstruktor f�r die Klasse Raumschiff
	 * 
	 * @param photonentorpedoAnzahl            ist die Anzahl der einsatzf�higen
	 *                                         Photonentorpedos
	 * @param energieversorgungInProzent       ist der Zustand der Energieversorgung
	 * @param schildeInProzent                 ist der Zustand des Schildes
	 * @param huelleInProzent                  ist der Zustand der H�lle
	 * @param lebenserhaltungssystemeInProzent ist der Zustand der
	 *                                         Lebenserhaltungssysteme
	 * @param androidenAnzahl                  ist der Anzahl der Reparaturandroiden
	 *                                         an Bord
	 * @param schiffsname                      ist der Name des Schiffs
	 */

	public raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	/**
	 * parameterloser Konstruktor f�r die Klasse Raumschiff
	 */

	public raumschiff() {

	}

	// Getter und Setter
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	// Methoden

	/**
	 * Methode zum hinzuf�gen einer Ladung ins Ladungsverzeichnis
	 * 
	 * @param neueLadung bezeichnet die hinzuzuf�gende Ladung
	 */

	public void addLadung(ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Methode zur Ausgabe des Zustand des Schiffs
	 */

	public void zustandSchiff() {
		System.out.println("Zustand des Schiffs: " + this.schiffsname);
		System.out.println("Photonentorpedos: " + this.photonentorpedoAnzahl);
		System.out.println("Energieversorgung(%): " + this.energieversorgungInProzent);
		System.out.println("Schilde(%): " + this.schildeInProzent);
		System.out.println("H�lle(%): " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme(%): " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Anzahl Androiden: " + this.androidenAnzahl);
		System.out.println("Schiffsname: " + this.schiffsname);
	}

	/**
	 * Methode zur Ausgabe des Ladungsverzeichnis des Schiffes in der Konsole.
	 */

	public void ladungSchiff() {
		System.out.println("Ladungsliste " + this.schiffsname);
		for (ladung ladung : ladungsverzeichnis) {
			System.out.println(ladung);
		}
	}

	/**
	 * Methode zum Verwendung eines Torpedos auf ein feindliches Raumschiff. Zudem
	 * wir jeweils eine Nachricht an alle abgegeben, entweder "Click" wenn nicht
	 * abgefeuert oder "Photonentorpedo abgeschossen" wenn abgefeuert.
	 * 
	 * @param zielschiff bezeichnet dabei den Namen des Zielschiff
	 */

	public void torpedoSchie�en(raumschiff zielschiff) {
		if (this.photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			trefferVermerken(zielschiff);
		}
	}

	/**
	 * Methode zum Einsatzvon Photonentorpedos unter Ber�cksichtigung der Ladung.
	 * Zudem wir jeweils eine Nachricht an alle abgegeben, entweder "Click" wenn
	 * nicht abgefeuert oder "X Photonentorpedos eingesetzt" wenn abgefeuert.
	 * 
	 * @param ladungTorpedos ist dabei die �bergebene Anzahl an verladenen Torpedos.
	 * @return gibt die neue Anzahl an verladenen Torpedos zur�ck.
	 */

	public int ladungPhotonentorpedosEinsetzen(int ladungTorpedos) {
		int neueLadung = 0;
		if (ladungTorpedos == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
		if (ladungTorpedos < this.photonentorpedoAnzahl) {
			System.out.println(this.photonentorpedoAnzahl + " Photonentorpedo(s) eingesetzt");
			neueLadung = ladungTorpedos - photonentorpedoAnzahl;
		} else {
			this.photonentorpedoAnzahl += ladungTorpedos;
			System.out.println(this.photonentorpedoAnzahl + " Photonentorpedo(s) eingesetzt");
			neueLadung = ladungTorpedos - photonentorpedoAnzahl;
		}
		return neueLadung;
	}

	/**
	 * Methode zum Abschie�en der Phaserkanone. Dies kann nur passieren, sollte die
	 * Energieversorgung gr��er als 50 sein. Zudem wir jeweils eine Nachricht an
	 * alle abgegeben, entweder "Click" wenn nicht abgefeuert oder "Phaserkanone
	 * abgeschossen" wenn abgefeuert.
	 * 
	 * @param zielschiff bezeichnet dabei den Namen des Zielschiff
	 */

	public void phaserkanoneSchie�en(raumschiff zielschiff) {
		if (this.energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			trefferVermerken(zielschiff);
		}
	}

	/**
	 * Methode zum registrieren eines Treffers. Dabei wird wenn m�glich nur das
	 * Schild um 50 reduziert, sollte dies auf 0 fallen wird dei H�lle und
	 * Energieversorgung um 50 reduziert. Sollte die H�lle auf 0 reduziert werden,
	 * wird die Lebenserhaltung auf 0 gesetzt und eine Nachricht �ber die
	 * vollst�ndige Zerst�rung des Schiffes wird an alle gesendet.
	 * 
	 * @param zielschiff bezeichnet dabei den Namen des Zielschiff.
	 */

	private void trefferVermerken(raumschiff zielschiff) {
		System.out.println(zielschiff.getSchiffsname() + " wurde getroffen!");
		zielschiff.setSchildeInProzent(zielschiff.schildeInProzent - 50);
		if (zielschiff.schildeInProzent == 0) {
			zielschiff.setHuelleInProzent(zielschiff.huelleInProzent - 50);
			zielschiff.setEnergieversorgungInProzent(zielschiff.energieversorgungInProzent - 50);
			if (zielschiff.huelleInProzent == 0) {
				zielschiff.setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Lebenserhaltungssysteme vollst�ndig vernichtet!");
			}
		}
	}

	/**
	 * Methode zur �bermittlung einer Nachricht an alle umkreisende Schiffe.
	 * 
	 * @param nachricht ist der String der die Nachricht enth�lt.
	 */

	public void nachrichtAnAlle(String nachricht) {
		this.broadcastKommunikator.add(nachricht);
	}

	/**
	 * Methode zum wiedergeben aller abgesendeten Nachrichten. Dabei werden alle
	 * Nachrichten in der Konsole ausgegeben.
	 */

	public void logbuchWiedergeben() {
		System.out.println("Broadcast Kommunikator des Schiffs: " + this.schiffsname);
		for (int i = 0; i < this.broadcastKommunikator.size(); i++)
			System.out.println(this.broadcastKommunikator.get(i));
	}

	/**
	 * Wenn die Menge einer Ladung 0 ist, dann wird das Objekt Ladung aus der Liste
	 * entfernt.
	 */

	public void ladungsverzeichnisAufr�umen() {
		int i = 0;
		ArrayList<ladung> toDelete = new ArrayList<>();
		while (i < ladungsverzeichnis.size()) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				toDelete.add(ladungsverzeichnis.get(i));
				System.out.println(ladungsverzeichnis.get(i));
			}
			i++;
		}
		ladungsverzeichnis.removeAll(toDelete);
	}

	/**
	 * Methode zur Erzeugung eines Boolschen Wertes f�r das Schild.
	 * 
	 * @return boolscher Wert True oder False abh�ngig vom Zustand des Schildes.
	 */

	public boolean reparaturSchild() {
		return schildeInProzent != 100;
	}

	/**
	 * Methode zur Erzeugung eines Boolschen Wertes f�r die H�lle.
	 * 
	 * @return boolscher Wert True oder False abh�ngig vom Zustand der H�lle.
	 */

	public boolean reparaturHuelle() {
		return huelleInProzent != 100;
	}

	/**
	 * Methode zur Erzeugung eines Boolschen Wertes f�r die Energieversorgung.
	 * 
	 * @return boolscher Wert True oder False abh�ngig vom Zustand der
	 *         Energieversorgung.
	 */

	public boolean reparaturEnergieversorgung() {
		return energieversorgungInProzent != 100;
	}

	/**
	 * Die Methode entscheidet anhand der �bergebenen Parameter, welche
	 * Schiffsstrukturen repariert werden sollen. Es wird eine Zufallszahl zwischen
	 * 0 - 100 erzeugt, welche f�r die Berechnung der Reparatur ben�tigt wird. Ist
	 * die �bergebene Anzahl von Androiden gr��er als die vorhandene Anzahl von
	 * Androiden im Raumschiff, dann wird die vorhandene Anzahl von Androiden
	 * eingesetzt. Prozentuale Berechnung der reparierten Schiffsstrukturen:
	 * (Zufallszahl * Anzahl Androiden) / Anzahl Schiffsstrukturen auf true. Das
	 * Ergebnis wird den auf true gesetzten Schiffsstrukturen hinzugef�gt.
	 * 
	 * @param schilde           ist ein boolscher Wert der bestimmt ob die Schilde
	 *                          repariert werden muss (true = ja)
	 * @param energieversorgung ist ein boolscher Wert der bestimmt ob die
	 *                          Energieversorgung repariert werden muss (true = ja)
	 * @param huelle            ist ein boolscher Wert der bestimmt ob die H�lle
	 *                          repariert werden muss (true = ja)
	 * @param anzahlDroiden     ist die Anzahl der �bergebenden Reparaturandroiden
	 */

	public void reperaturAndroidenEinsetzen(boolean schilde, boolean energieversorgung, boolean huelle,
			int anzahlDroiden) {
		Random zufall = new Random();
		int zufallsZahl = zufall.nextInt(100);
		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}
		int anzahlTrue = 0;
		if (schilde == true) {
			anzahlTrue += 1;
		}
		if (energieversorgung == true) {
			anzahlTrue += 1;
		}
		if (huelle == true) {
			anzahlTrue += 1;
		}

		int berechnung = (zufallsZahl * anzahlDroiden) / anzahlTrue;

		if (huelle == true) {
			this.setHuelleInProzent(huelleInProzent + berechnung);
		}
		if (schilde == true) {
			this.setSchildeInProzent(schildeInProzent + berechnung);
		}
		if (energieversorgung == true) {
			this.setEnergieversorgungInProzent(energieversorgungInProzent + berechnung);
		}
	}
}
