package Raumschiff;

public class raumschiff_test {

	public static void main(String[] args) {
		
		//Initialisierung der Ladungen
		ladung schneckensaft = new ladung ("Ferengi Schneckensaft", 200);
		ladung schrott = new ladung ("Borg-Schrott", 5);
		ladung materie = new ladung ("Rote Materie", 2);
		ladung sonde = new ladung ("Forschungssonde", 35);
		ladung schwert = new ladung ("Bet'leth Klingonen Schwert", 200);
		ladung plasma = new ladung ("Plasma-Waffe", 50);
		ladung torpedo = new ladung ("Photonentorpedo", 3);
		
		//Initialisierung der Raumschiffe
		
		raumschiff klingonen = new raumschiff (1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		raumschiff romulaner = new raumschiff (2, 100, 100, 100, 100, 2, "IRW Khazara");
		raumschiff vulkanier = new raumschiff (0, 80, 80, 50, 100, 5, "Ni'Var");
		
		//Zuweisung der Ladung
		
		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);
		
		romulaner.addLadung(schrott);
		romulaner.addLadung(materie);
		romulaner.addLadung(plasma);
		
		vulkanier.addLadung(sonde);
		vulkanier.addLadung(torpedo);
		
		//Ausführende Methoden
		
		//Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.
		klingonen.torpedoSchießen(romulaner);
		
		//Die Romulaner schießen mit der Phaserkanone zurück.
		romulaner.phaserkanoneSchießen(klingonen);
		
		//Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		//Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		klingonen.zustandSchiff();
		klingonen.ladungSchiff();
		
		//Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein (für Experten).
		vulkanier.reperaturAndroidenEinsetzen(vulkanier.reparaturSchild(), vulkanier.reparaturEnergieversorgung(), vulkanier.reparaturHuelle(), vulkanier.getAndroidenAnzahl());
		
		//Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf (für Experten).
		
		torpedo.setMenge(vulkanier.ladungPhotonentorpedosEinsetzen(torpedo.getMenge()));
		vulkanier.ladungsverzeichnisAufräumen();
		
		//Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
		klingonen.torpedoSchießen(romulaner);
		klingonen.torpedoSchießen(romulaner);
		
		//Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		klingonen.zustandSchiff();
		klingonen.ladungSchiff();
		romulaner.zustandSchiff();
		romulaner.ladungSchiff();
		vulkanier.zustandSchiff();
		vulkanier.ladungSchiff();
		
		//Geben Sie den broadcastKommunikator aus.
		
		klingonen.logbuchWiedergeben();
		romulaner.logbuchWiedergeben();
		vulkanier.logbuchWiedergeben();
	}

}
