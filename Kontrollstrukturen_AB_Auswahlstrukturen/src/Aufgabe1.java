import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		
		Scanner kb = new Scanner(System.in);
		//1. Teil
		System.out.println("Bitte geben Sie die erste Zahl ein!");
		byte zahl1_1 = kb.nextByte();
		
		System.out.println("Bitte geben Sie die zweite Zahl ein!");
		byte zahl2_1 = kb.nextByte();
		
		if(zahl1_1 == zahl2_1) {
			System.out.println("Zahl 1 entspricht Zahl 2.");
		}
		
		if(zahl2_1 > zahl1_1) {
			System.out.println("Zahl 2 ist gr��er als Zahl 1.");
		}
		
		else if(zahl1_1 > zahl2_1){
			System.out.println("Zahl 1 ist gr��er als Zahl 2.");
		}
		
		System.out.println("///////2. Teil///////");
		
		//2. Teil
		System.out.println("Bitte geben Sie die erste Zahl ein!");
		byte zahl1_2 = kb.nextByte();
		
		System.out.println("Bitte geben Sie die zweite Zahl ein!");
		byte zahl2_2 = kb.nextByte();
		
		System.out.println("Bitte geben Sie die dritte Zahl ein!");
		byte zahl3_2 = kb.nextByte();
		
		if(zahl1_2 > zahl2_2 && zahl1_2 > zahl3_2) {
			System.out.println("Zahl 1 ist die gr��te Zahl von allen.");
		}
		
		if(zahl3_2 > zahl2_2 || zahl3_2 > zahl1_2) {
			System.out.println("Zahl 3 ist gr��er als eine der beiden anderen.");
		}
		
		if(zahl1_2 > zahl2_2 && zahl1_2 > zahl3_2) {
			System.out.println(zahl1_2);
		}
		
		if(zahl2_2 > zahl1_2 && zahl2_2 > zahl3_2) {
			System.out.println(zahl2_2);
		}
		
		else if(zahl3_2 > zahl2_2 && zahl3_2 > zahl1_2) {
			System.out.println(zahl3_2);
		}
	}

}
