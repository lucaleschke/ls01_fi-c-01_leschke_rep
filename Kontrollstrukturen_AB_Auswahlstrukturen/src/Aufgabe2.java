import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {

		Scanner kb = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie den Nettobetrag ein!");
		double netto = kb.nextDouble();
		
		System.out.println("Bitte entscheiden Sie sich f�r einen Steuersatz (j = erm��igt n = voll)!");
		char steuersatz = kb.next().charAt(0);
		
		double voll = 1.19;
		double erm��igt = 1.07;
		
		if(steuersatz == 'j') {
			double brutto = netto * erm��igt;
			System.out.println(brutto);
		}
		
		if(steuersatz == 'n') {
			double brutto = netto * voll;
			System.out.println(brutto);
		}
		
		else if(steuersatz != 'j' || steuersatz != 'n') {
			System.out.println("Bitte w�hlen Sie einen g�ltigen Steuersatz!");
		}
	}
}
