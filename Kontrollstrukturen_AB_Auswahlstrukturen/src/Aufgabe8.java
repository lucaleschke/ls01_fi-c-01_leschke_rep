import java.util.Scanner;

public class Aufgabe8 {

	public static void main(String[] args) {
		
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Jahreszahl ein!");
		
		int jahr = kb.nextInt();
				
		if(jahr < 1582 && jahr > 45) {
			if(jahr%4 == 0) {
				System.out.println("Das Jahr " + jahr + " ist ein Schaltjahr!");
			}
			else {
				System.out.println("Das Jahr " + jahr + " ist kein Schaltjahr!");
			}
		}
		
		else if(jahr >= 1582) {
			if(jahr%4 == 0 && jahr%100 != 0 || jahr%400 == 0) {
				System.out.println("Das Jahr " + jahr + " ist ein Schaltjahr!");
			}
			else {
				System.out.println("Das Jahr " + jahr + " ist kein Schaltjahr!");
			}
				
		}
		
		else {
			System.out.println("Das Schaltjahrsystem wurde erst 45 n. Chr. durch C�sar eingef�hrt!");
		}
		
	}

}
