import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String []args) {
		
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie einen Wert f�r x ein!");
		
		double x = kb.nextDouble();
		final double e = 2.718;
		
		if(x <= 0) {
			System.out.println("exponentiell");
			double funktionswert_ex = Math.pow(e, x);
			System.out.println(funktionswert_ex);
		}
		
		if(0 < x && x <= 3) {
			System.out.println("quadratisch");
			double funktionswert_q = Math.pow(x, 2) + 1;
			System.out.println(funktionswert_q);
		}
		
		else if(x > 3) {
			System.out.println("linear");
			double funktionswert_l = 2 * x + 4;
			System.out.println(funktionswert_l);
		}
	}
	
}
