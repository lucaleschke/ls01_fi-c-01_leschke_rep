import java.util.Arrays;
import java.util.Scanner;

public class RömischeZahlen2 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		int dezimalZahl = 0;
		
		System.out.println("Bitte geben eine römische Zahl ein! (Maximal 3999)");
		String roemZahl = tastatur.next();
		String[] einzelneZahlen = new String[roemZahl.length()];
			
		for (int i = 0; i < einzelneZahlen.length; i++) {
			einzelneZahlen[i] = roemZahl.valueOf(roemZahl.charAt(i));// Speicherung des Strings an der Stelle i
			}
		
		for(int i = 0; i < einzelneZahlen.length; i++){
			String roemZahlAr = einzelneZahlen[i];
			
			switch (roemZahlAr) {
			case "M":
				dezimalZahl += 1000;
				break;
				
			case "D":
				dezimalZahl += 500;
				break;				
			}
		}
		
		System.out.println(Arrays.toString(einzelneZahlen));
		System.out.println(dezimalZahl);
		
		}
		
		
}



