import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
	
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Einzelpreis der Artikel:");
		double einzelpreis = tastatur.nextDouble();
		System.out.println("Anzahl der Artikel:");
		byte anzahl = tastatur.nextByte();
		
		if(anzahl < 10) {
			double gesamtpreis = (anzahl * einzelpreis + 10) * 1.19;
			System.out.printf("Der Gesamtbetrag betr�gt %.2f Euro!", gesamtpreis);
		}
		else if (anzahl >= 10) {
			double gesamtpreis = (anzahl * einzelpreis) * 1.19;
			System.out.printf("Der Gesamtbetrag betr�gt %.2f Euro!", gesamtpreis);
		}
		else {
			System.out.println("Fehler");
		}
				
	}

}
