import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		
		System.out.println("Bitte geben Sie eine Note ein!");
		
		Scanner tastatur = new Scanner(System.in);
		byte note = tastatur.nextByte();
		
		if (note == 1) 
			System.out.print("Sehr gut");
		
		else if (note == 2) 
			System.out.print("Gut");
		
		else if (note == 3) 
			System.out.print("Befriedigend");
				
		else if (note == 4) 
			System.out.print("Ausreichend");
			
		else if (note == 5) 
			System.out.print("Mangelhaft");
				
		else if (note == 6) 
			System.out.print("Ungenügend");
				
		/*else if (note != 1 || note != 2 || note != 3 || note != 4 || note != 5 || note != 6) 
			System.out.print("Unzulässige Note");*/
		
		else 
			System.out.println("Unzulässige Note");
		}
}
