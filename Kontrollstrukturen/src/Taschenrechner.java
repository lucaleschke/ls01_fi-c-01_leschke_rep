import java.util.Scanner;

public class Taschenrechner {

	public static double zahleneingabe() {
		double zahl;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl ein!");
		zahl = tastatur.nextDouble();
		return zahl;
	}
	
	public static char operatoreingabe() {
		char operator;
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie einen Rechenoperator ein!");
		operator = tastatur.next().charAt(0);
		return operator;
	}
	
	public static double berechnung(double x, double y, char operator) {
		double ergebnis = 0;
		
		switch (operator) {
		case '/':
			ergebnis = x / y;
			break;
			
		case '*':
			ergebnis = x * y;
			break;
			
		case '+':
			ergebnis = x + y;
			break;
			
		case '-':
			ergebnis = x - y;
			break;
			
		default:
			System.out.println("Bitte w�hlen Sie einen g�ltigen Rechenoperator aus!");
		}
		
		return ergebnis;
		
				
	}
	public static void main(String[] args) {
		double zahl_1 = zahleneingabe();
		char operator = operatoreingabe();
		double zahl_2 = zahleneingabe();
		double ergebnis = berechnung(zahl_1, zahl_2, operator);
		System.out.printf("Das Ergebnis der Berechnung lautet: %.2f" , ergebnis);
	}


}
