import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie einen Nettowert ein.");
		double netto = tastatur.nextDouble();
		System.out.println("Soll der erm��igte Steuersatz angewendet werden?");
		char abfrage = tastatur.next().charAt(0);
		double steuersatz;
		double brutto;
		
		if (abfrage == 'j') {
			steuersatz = 1.07;
			brutto = netto * steuersatz;
			System.out.println(brutto);
		}
		
		else if(abfrage == 'n') {
			steuersatz = 1.19;
			brutto = netto * steuersatz;
			System.out.println(brutto);
		}
		
		else {
			System.out.println("Bitte nur j oder n eingeben!");
		}
		
	}

}
