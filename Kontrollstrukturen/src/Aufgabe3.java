import java.util.Scanner;

public class Aufgabe3 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		//Aufgabe 3a
		System.out.println("Bitte geben Sie einen Monat ein.");
		byte monat = tastatur.nextByte();
		
		if (monat == 1) {
			System.out.println("Januar");
		}
		else if (monat == 2) {
			System.out.println("Februar");
		}
		else if (monat == 3) {
			System.out.println("M�rz");
		}
		else if (monat == 4) {
			System.out.println("April");
		}
		else if (monat == 5) {
			System.out.println("Mai");
		}
		else if (monat == 6) {
			System.out.println("Juni");
		}
		else if (monat == 7) {
			System.out.println("Juli");
		}
		else if (monat == 8) {
			System.out.println("August");
		}
		else if (monat == 9) {
			System.out.println("September");
		}
		else if (monat == 10) {
			System.out.println("Oktober");
		}
		else if (monat == 11) {
			System.out.println("November");
		}
		else if (monat == 12) {
			System.out.println("Dezember");
		}
		else {
			System.out.println("Fehler");
		}
		
		//Aufgabe 3b
		
		System.out.println("Bitte geben Sie einen Monat ein.");
		byte monat2 = tastatur.nextByte();
		
		switch (monat2) {
		case 1:
			System.out.println("Januar");
			break;
			
		case 2:
			System.out.println("Februar");
			break;
			
		case 3:
			System.out.println("M�rz");
			break;
			
		case 4:
			System.out.println("April");
			break;
			
		case 5:
			System.out.println("Mai");
			break;
			
		case 6:
			System.out.println("Juni");
			break;
			
		case 7:
			System.out.println("Juli");
			break;
			
		case 8:
			System.out.println("August");
			break;
			
		case 9:
			System.out.println("September");
			break;
			
		case 10:
			System.out.println("Oktober");
			break;
			
		case 11:
			System.out.println("November");
			break;
			
		case 12:
			System.out.println("Dezember");
			break;
			
		default:
			System.out.println("Der Wert liegt nicht zwischen 1 und 12");
		
		}
		
		}

}
