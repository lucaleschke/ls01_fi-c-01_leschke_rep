import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie Ihr Gewicht in kg an!");
		int gewicht = tastatur.nextInt();
		System.out.println("Bitte geben Sie Ihre K�rpergr��e in cm an!");
		int gr��e = tastatur.nextInt();
		System.out.println("Bitte w�hlen Sie ihr Geschlecht (m/w) aus!");
		char geschlecht = tastatur.next().charAt(0);
						
		float bmi = gewicht /  (gr��e/100^2);
		System.out.println("Der BMI liegt bei: " + bmi);
		System.out.print("Das entspricht: ");
		
		if(geschlecht == 'm') {
			if (bmi < 20) {
				System.out.println("Untergewicht");
			}
			else if (bmi > 25) {
				System.out.println("�bergewicht");
			}
			else{
				System.out.println("Normalgewicht");
			}
		}
		else if(geschlecht == 'w') {
			if (bmi < 19) {
				System.out.println("Untergewicht");
			}
			else if (bmi > 24) {
				System.out.println("�bergewicht");
			}
			else{
				System.out.println("Normalgewicht");
			}
		}
		else {
			System.out.println("Bitte geben Sie als Geschlecht m oder w an!");
		}
	}

}
