package Angestellter;

public class Angestellter {

	private String name;
	private double gehalt;
	private String vorname;
	private String vollname;
	// TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu
	// und implementieren Sie die entsprechenden get- und set-Methoden.
	// TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	// TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen
	// initialisiert.

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	// 3. TODO
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	// 3. TODO
	public String getVorname() {
		return this.vorname;
	}

	// 4. TODO
	public Angestellter(String vorname, String name, double gehalt) {
		// this.name = name;
		// this.vorname = vorname;
		this(vorname, name);
		setGehalt(gehalt);
	}

	// 5. TODO
	public Angestellter(String vorname, String name) {
		this.name = name;
		this.vorname = vorname;
	}

	public void setGehalt(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methoden.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if (gehalt > 0) {
			this.gehalt = gehalt;
		} else
			System.out.println("negativer Gehaltsbetrag");
	}

	public double getGehalt() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		return this.gehalt;
	}
	// TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Zuname) als string zur�ckgibt.

	public String vollname() {
		this.vollname = getVorname() + " " + getName();
		return this.vollname;
	}
}