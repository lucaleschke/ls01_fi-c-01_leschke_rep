package Angestellter;

public class TestAngestellter {

	public static void main(String[] args) {

		// Erzeugen der Objekte
		Angestellter ang1 = new Angestellter("Manfred", "Meier", 4500);
		Angestellter ang2 = new Angestellter("Petersen", "Peter", -6000);
		/*
		 * TODO: 7. Erzeugen Sie ein zusaetzliches Objekt ang3 und geben Sie es auch auf
		 * der Konsole aus, die Attributwerte denken Sie sich aus.
		 */
		Angestellter ang3 = new Angestellter("Luca", "Leschke", 1870);

		/*
		 * TODO: 8. Erzeugen Sie zwei zusaetzliche Objekte ang4 und ang5 mit dem
		 * Konstruktor, der den Namen und Vornamen initialisiert, die Attributwerte
		 * denken Sie sich aus.
		 */
		Angestellter ang4 = new Angestellter("Hans", "Peter");
		Angestellter ang5 = new Angestellter("Dieter", "Dieterson");

		// Setzen der Attribute
		/*
		 * TODO: 9. Fuegen Sie ang4 und ang5 jeweils ein Gehalt hinzu, die Attributwerte
		 * denken Sie sich aus. Geben Sie ang4 und ang5 auch auf dem Bildschirm aus.
		 */
		ang4.setGehalt(9000);
		ang5.setGehalt(187000);

		// Bildschirmausgabe
		System.out.println("Name: " + ang1.getName());
		System.out.println("Vorname: " + ang1.getVorname());
		System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
		System.out.println("Vollname: " + ang1.vollname());
		System.out.println("\nName: " + ang2.getName());
		System.out.println("Vorname: " + ang2.getVorname());
		System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
		System.out.println("Vollname: " + ang2.vollname());
		System.out.println("\nName: " + ang3.getName());
		System.out.println("Vorname: " + ang3.getVorname());
		System.out.println("Gehalt: " + ang3.getGehalt() + " Euro");
		System.out.println("Vollname: " + ang3.vollname());
		// 9. TODO
		System.out.println("\nName: " + ang4.getName());
		System.out.println("Vorname: " + ang4.getVorname());
		System.out.println("Gehalt: " + ang4.getGehalt() + " Euro");
		System.out.println("Vollname: " + ang4.vollname());

		System.out.println("\nName: " + ang5.getName());
		System.out.println("Vorname: " + ang5.getVorname());
		System.out.println("Gehalt: " + ang5.getGehalt() + " Euro");
		System.out.println("Vollname: " + ang5.vollname());

	}

}
