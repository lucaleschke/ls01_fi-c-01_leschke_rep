import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
	
		Scanner tastatur = new Scanner(System.in); 
		System.out.println(text);
		String artikel = tastatur.next();
		return artikel;
	}
	
	public static int liesInt(String text) {
		
		Scanner tastatur = new Scanner(System.in); 
		System.out.println(text);
		int anzahl = tastatur.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		
		Scanner tastatur = new Scanner(System.in); 
		System.out.println(text);
		double preis = tastatur.nextDouble();
		return preis;
	}
	
	public static double berechneNettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtpreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst);
	}
	
	public static void rechnungsausgabe (String artikel, int anzahl, double netto, double brutto, double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, netto);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.2f%s)%n", artikel, anzahl, brutto, mwst, "%");
	}

	public static void main(String[] args) {
		
		final double mwst = 0.19;
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double einzelpreis = liesDouble("Geben Sie den Einzelpreis ein:");
		double netto = berechneNettopreis(anzahl, einzelpreis);
		double brutto = berechneGesamtpreis(netto, mwst);
		rechnungsausgabe(artikel, anzahl, netto, brutto, mwst);
		
	}

}